<?php

use App\Controllers\HomeController;

$router->get('/', [App\Controllers\HomeController::class, 'index']);
$router->get('/about', [HomeController::class, 'about']);
$router->get('/create/user', [HomeController::class, 'create']);