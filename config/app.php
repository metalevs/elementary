<?php

return [
    /**
     * App base url including trailing slash.
     * Example: https://mysite.com/
     */
    'app_url' => 'http://framework.local/',
];