<?php

return [
    PDO::class => [
        'constructor' => [
            'dsn' => 'mysql:host=127.0.0.1;dbname=framework',
            'username' => 'root',
            'passwd' => '',
            'options' => [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
        ]
    ],

    Elementary\Http\Request::class => [
        'constructor' => [
            'post' => $_POST,
            'get' => $_GET,
            'cookies' => $_COOKIE,
            'files' => $_FILES,
            'server' => $_SERVER
        ]
    ],

    Elementary\View\Template::class => [
        'constructor' => [
            'path' => __DIR__ . '/../templates/',
            'extensions' => [new Elementary\View\Extensions\Output(), new Elementary\View\Extensions\Url('http://framework.local/')]
        ]
    ]
];

