<?php declare(strict_types = 1);

use Elementary\Routing\Router;
use Elementary\Http\Request;
use Elementary\Http\Response;
use Elementary\DI\Injector;
use Elementary\DI\MemoryCache;

/**
 * Utilize composer autoload library.
 */
require __DIR__ . '/../vendor/autoload.php';

/**
 * Debug helpers.
 */
require __DIR__ . '/../elementary/Helpers/Functions.php';

/**
 * Load DI container.
 */
$injector = new Injector(new MemoryCache);

/**
 * Load class config for the DI container.
 */
$injector->setRules(require __DIR__ . '/../config/rules.php');

/**
 * Configure router.
 */
$router = new Router();
require __DIR__ . '/../config/routes.php';

/**
 * Request and response objects
 */
$request = $injector->make(Request::class);
$response = $injector->make(Response::class);

/**
 * Dispatch request.
 */
$routeInfo = $router->dispatch($request->getMethod(), $request->getUri());

/**
 * If route was found instantiate controller.
 */
switch ($routeInfo['status']) {

    case Router::FOUND:
    $controller = $injector->make($routeInfo['controller']);
    $response = $controller->{$routeInfo['method']}(...$routeInfo['params']);
    break;

    case Router::NOT_FOUND:
    $response->setStatusCode(404);
    $response->setContent('Page you are looking for could not be found');
    break;
}

/**
 * Check if controller returned a response object.
 */
if (!$response instanceof Response) {
    throw new Exception('Controller methods must return a Response object');
}

/**
 * Send headers.
 */
foreach ($response->getHeaders() as $header) {
    header($header, false);
}

/**
 * Finally output the response content.
 */
echo $response->getContent();


