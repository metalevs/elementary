<?php declare(strict_types = 1);

namespace Elementary\DI;

use Closure;

class MemoryCache
{
    /**
     * Cached object list.
     *  
     * @var array
     */
    private $cache = [];

    /**
     * Check if cache contains instance.
     * 
     * @param  string  $name
     * @return boolean
     */
    public function has(string $name): bool
    {
        return array_key_exists($name, $this->cache);
    }

    /**
     * Set object instance to in memory cache.
     * 
     * @param string $name
     * @param object $instance
     */
    public function set(string $name, object $instance): void
    {
        $this->cache[$name] = $instance;
    }

    /**
     * Set annonymous function 
     * 
     * @param string   $name     [description]
     * @param callable $callable [description]
     */
    public function setClosure(string $name, closure $callable):void
    {
        $this->cache[$name] = $callable;
    }

    /**
     * Get object from memory.
     * 
     * @param  string $name
     * @return object
     */
    public function get(string $name): object
    {
        return $this->cache[$name]();
    }
}