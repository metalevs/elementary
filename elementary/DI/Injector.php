<?php declare(strict_types = 1);

namespace Elementary\DI;

use Elementary\DI\Exceptions\ContainerException;
use Elementary\DI\Cache;
use ReflectionClass, ReflectionMethod;

class Injector
{
    /**
     * Rules which will be used in object construction.
     * 
     * @var array
     */
    private $rules = [];

    /**
     * Caching object.
     * 
     * @var Elementary\DI\MemoryCache
     */
    private $cache;

    /**
     * Set caching object.
     * 
     * @param MemoryCache $cache
     */
    public function __construct(MemoryCache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Set rules which will be used  to construct our objects.
     * 
     * @param array $rules
     */
    public function setRules(array $rules): void
    {
        $this->rules =  $rules;
    }

    /**
     * Create fully constructed object.
     * 
     * @param  string $className
     * @return Object
     */
    public function make(string $className): object
    {
        if ($this->cache->has($className)) {
            return $this->cache->get($className);
        }

        $className = $this->rules['alias'][$className] ?? $className;

        $constructor = $this->getConstructor($className);
        $dependencies = [];

        if($constructor){
            $dependencies = $this->resolveDependencies($className, $constructor);
        }

        if (isset($this->rules[$className]['shared']) && $this->rules[$className]['shared'] !== false) {
            $this->cache->set($className, new $className(...$dependencies));
        } else {
            $this->cache->setClosure($className, function () use ($dependencies, $className){
                return new $className(...$dependencies);
            });
        }

        return  $this->cache->get($className);
    }

    /**
     * Resolve constructor parameters.
     * 
     * @param  string $className
     * @param  ReflectionMethod $constructor
     * @return array
     */
    private function resolveDependencies($className, ReflectionMethod $constructor): array
    {
        $constructorParams = [];
        
        foreach ($constructor->getParameters() as $parameter) {

            $class = $parameter->getClass() ? $parameter->getClass()->name : null;

            if ($class) {
                $constructorParams[] = $this->make($class);
                continue;
            }

            if (isset($this->rules[$className]['constructor'][$parameter->getName()])) {
                $constructorParams[] = $this->rules[$className]['constructor'][$parameter->getName()];
            }

        }

        return $constructorParams;
    }

    /**
     * Get class constructor.
     * 
     * @param  string $className
     * @return ReflectionMethod
     */
    private function getConstructor($className): ?ReflectionMethod
    {
        $reflectionClass = new ReflectionClass($className);

        if (!$reflectionClass->isInstantiable()) {
            throw new ContainerException(sprintf('%s is not instantiable', $className));
        }

        return $reflectionClass->getConstructor();
    }
}