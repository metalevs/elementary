<?php

namespace Elementary\DI\Exceptions;

class ContainerException extends \Exception{}