<?php declare(strict_types = 1);

namespace Elementary\Routing;

class Router
{
    /**
     *  Match found
     */
    const FOUND = 1;

    /**
     * Match not found.
     */
    const NOT_FOUND = 0;

    /**
     * List of routes to be matched against.
     * 
     * @var array
     */
    private $routes = [];

    /**
     * Holds matched controller data.
     * 
     * @var array
     */
    private $match = [];

    /**
     * Predefined common patterns.
     * 
     * @var array
     */
    private $patterns = [
        ':num' => '[0-9]+',
        ':string' => '[a-zA-Z\-_]+',
        ':any' => '[a-zA-Z0-9\_-]+'
    ];

    /**
     * Shorthand for a GET route.
     * 
     * @param  string $pattern
     * @param  array $handler
     * @return void
     */
    public function get(string $pattern, array $handler): void
    {
        $this->add(['GET'], $pattern, $handler);
    }

    /**
     * Shorthand for a POST route.
     * 
     * @param  string $pattern
     * @param  array $handler
     * @return void
     */
    public function post(string $pattern, array $handler): void
    {
        $this->add(['POST'], $pattern, $handler);
    }

    /**
     * Shorthand for a PATCH route.
     * 
     * @param  string $pattern
     * @param  array $handler
     * @return void
     */
    public function patch(string $pattern, array $handler): void
    {
        $this->add(['PATCH'], $pattern, $handler);
    }

    /**
     * Shorthand for a DELETE route.
     * 
     * @param  string $pattern
     * @param  array $handler
     * @return void
     */
    public function delete(string $pattern, array $handler): void
    {
        $this->add(['DELETE'], $pattern, $handler);
    }

    /**
     * Shorthand for a PUT route.
     * 
     * @param  string $pattern
     * @param  array $handler
     * @return void
     */
    public function put(string $pattern, array $handler): void
    {
        $this->add(['PUT'], $pattern, $handler);
    }

    /**
     * Shorthand for a any request route.
     * 
     * @param  string $pattern
     * @param  array $handler
     * @return void
     */
    public function any(string $pattern, array $handler): void
    {
        $this->add(['PUT', 'PATCH', 'DELETE', 'GET', 'POST'], $pattern, $handler);
    }

    /**
     * Create route.
     * 
     * @param array  $methods
     * @param string $pattern
     * @param array $handler
     */
    public function add(array $methods, string $pattern, array $handler): void
    {
        foreach ($methods as $method) {

            $this->routes[$method][] = [
                'pattern' => $pattern,
                'handler' => $handler
            ];

        }
    }

    /**
     * Match request to the pattern.
     * 
     * @param  string $method
     * @param  string $uri
     * @return array|null
     */
    public function dispatch(string $method, string $uri): array
    {
        $searches = array_keys($this->patterns);
        $replaces = array_values($this->patterns);

        if (isset($this->routes[$method])) {

            foreach ($this->routes[$method] as $route) {

                if (strpos($route['pattern'], ':') !== false) {
                    $route['pattern'] = str_replace($searches, $replaces, $route['pattern']);
                }

                if (!preg_match('#^' . $route['pattern'] . '$#', $uri, $matches)) {
                    continue;
                }

                return $this->match = [
                    'status' => self::FOUND,
                    'controller' => $route['handler'][0], 
                    'method' => $route['handler'][1], 
                    'params' => array_slice($matches, 1)
                ];
            }
        }

        return ['status' => self::NOT_FOUND];
    }
}