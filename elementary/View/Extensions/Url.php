<?php declare(strict_types = 1);

namespace Elementary\View\Extensions;

use Elementary\View\Interfaces\ExtensionInterface;
use Elementary\View\Template;

class Url implements ExtensionInterface
{
    /**
     * Website base url.
     * 
     * @var string
     */
    private $baseUrl;

    /**
     * Set base url.
     * 
     * @param string $baseUrl
     */
    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Register extension.
     * 
     * @param  Template $template
     * @return void
     */
    public function register(Template $template): void
    {
        $template->register('url', $this);
    }

    /**
     * Get base url.
     * 
     * @return string
     */
    public function baseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * Return asset url.
     * 
     * @param  string $value
     * @return string
     */
    public function link(string $value): string
    {
        return $this->baseUrl . $value;
    }
}