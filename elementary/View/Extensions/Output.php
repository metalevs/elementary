<?php declare(strict_types = 1);

namespace Elementary\View\Extensions;

use Elementary\View\Interfaces\ExtensionInterface;
use Elementary\View\Template;

class Output implements ExtensionInterface
{
    /**
     * Register extension.
     * 
     * @param  Template $template
     * @return void
     */
    public function register(Template $template): void
    {
        $template->register('output', $this);
    }

    /**
     * Escape variable.
     * 
     * @param  string $value
     * @return string
     */
    public function escape(string $value): string
    {
        return htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false);
    }
}