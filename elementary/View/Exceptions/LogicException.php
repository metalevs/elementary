<?php declare(strict_types = 1)

namespace Volt\View\Exceptions;

class LogicException extends \Exception{}