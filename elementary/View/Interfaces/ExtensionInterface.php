<?php declare(strict_types = 1);

namespace Elementary\View\Interfaces;

use Elementary\View\Template;

interface ExtensionInterface
{
    public function register(Template $template);
}