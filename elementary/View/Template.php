<?php declare(strict_types = 1);

namespace Elementary\View;

use Elementary\View\Interfaces\ExtensionInterface;

class Template
{
    /**
     * Path to the view file.
     * 
     * @var string
     */
    private $path;

    /**
     * View file extension default is .php
     * 
     * @var string
     */
    private $ext;

    /**
     * Name of the layout to extend from.
     * 
     * @var string
     */
    private $extend;

    /**
     * List of sections.
     * 
     * @var array
     */
    private $sections = [];

    /**
     * Parsed section data.
     * 
     * @var array
     */
    private $parsedSections = [];

    /**
     * Create new Pure instance.
     * 
     * @param string $path
     * @param string $ext
     */
    public function __construct(string $path, array $extensions, string $ext = '.php')
    {
        $this->path = $path;
        $this->ext = $ext;

        foreach ($extensions as $extension) {
            if ($extension instanceof ExtensionInterface) {
                $extension->register($this);
            }
        }
    }

    /**
     * Render template.
     * 
     * @param  string     $view
     * @param  array|null $data
     * @return string
     */
    public function render(string $view, array $data = null): string
    {
        if ($data !== null) {
            extract($data);
        }

        ob_start();
        require($this->path . $view . $this->ext);
        $result = ob_get_clean();

        if ($this->extend) {

            $name = $this->extend;
            $this->extend = null;

            $result = $this->render($name, $data);

        }

        return $result;
    }

    /**
     * Define template section.
     * 
     * @param  string $name
     * @return void
     */
    public function section(string $name): void
    {
        $this->sections[] = $name;
        ob_start();
    }

    /**
     * Define end of template section.
     * 
     * @return void
     */
    public function end(): void
    {
        $section = array_pop($this->sections);
        $this->parsedSections[$section] = ob_get_clean();
    }

    /**
     * Get defined section.
     * 
     * @param  string $name
     * @return mixed
     */
    public function get(string $name): ?string
    {
        if (array_key_exists($name, $this->parsedSections)) {
            return $this->parsedSections[$name];
        }

        return null;
    }

    /**
     * Extend layout.
     * 
     * @param  string $name
     * @return void
     */
    public function extends(string $name): void
    {
        $this->extend = $name;
    }

    /**
     * Register extension.
     * 
     * @param  ExtensionInterface $extension
     * @return void
     */
    public function register(string $name, ExtensionInterface $extension): void
    {
        $this->$name = $extension;
    }
}