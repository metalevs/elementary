<?php declare(strict_types = 1);

namespace Elementary\Http;

class Response
{   
    /**
     * Version.
     * 
     * @var string
     */
    private $version = '1.1';

    /**
     * Status code.
     * 
     * @var integer
     */
    private $statusCode = 200;

    /**
     * Status text.
     * 
     * @var string
     */
    private $statusText = 'OK';

    /**
     * All of the headers.
     * 
     * @var array
     */
    private $headers = [];

    /**
     * Body of the response.
     * 
     * @var string
     */
    private $content;

    /**
     * Available response codes.
     * 
     * @var array
     */
    private $statusTexts = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        208 => 'Already Reported',
        226 => 'IM Used',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Reserved',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Reserved for WebDAV advanced collections expired proposal',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        510 => 'Not Extended',
        511 => 'Network Authentication Required',
    ];

    /**
     * Set response status code and text.
     * 
     * @param int         $statusCode
     * @param string|null $statusText
     */
    public function setStatusCode(int $statusCode, string $statusText = null)
    {
        if($statusText == null & array_key_exists($statusCode, $this->statusTexts)){
            $statusText = $this->statusTexts[$statusCode];
        }

        $this->statusCode = $statusCode;
        $this->statusText = $statusText;
    }

    /**
     * Get response status code.
     * 
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Add header without overwriting.
     * 
     * @param string $name
     * @param string $value
     */
    public function addHeader(string $name, string $value): void
    {
        $this->headers[$name][] = $value;
    }

    /**
     * Overwrite header.
     *  
     * @param string $name
     * @param string $value
     */
    public function setHeader(string $name, string $value): void
    {
        $this->headers[$name] = [$value];
    }

    /**
     * Get all headers.
     * 
     * @return array
     */
    public function getHeaders(): array
    {
        return array_merge($this->getLineHeaders(), $this->getStandardHeaders());
    }

    /**
     * Set response body.
     * 
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * Get body of the response.
     * 
     * @return mixed
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Redirect to url
     * 
     * @param  string $url
     * @return void
     */
    public function redirect(string $url): void
    {
        $this->setHeader('location', $url);
        $this->setStatusCode(302);
    }

    /**
     * Get headers.
     * 
     * @return array
     */
    private function getStandardHeaders(): array
    {
        $headers = [];

        foreach ($this->headers as $name => $values) {
            foreach($values as $value){
                $headers[] = "$name: $value";
            }
        }

        return $headers;
    }

    /**
     * Get request line headers.
     * 
     * @return array
     */
    private function getLineHeaders(): array
    {
        $line = sprintf('HTTP/%s %s %s', $this->version, $this->statusCode, $this->statusText);
        $headers[] = trim($line);

        return $headers;
    }
}