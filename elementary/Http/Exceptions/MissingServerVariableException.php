<?php declare(strict_types = 1);

namespace Elementary\Http\Exceptions;
use Exception;

class MissingServerVariableException extends Exception
{
    public function __construct($variableName, $code = 0, Exception $previous = null)
    {
        $message = "Request server variable $variableName was not set";
        parent::__construct($message, $code, $previous);
    }
}