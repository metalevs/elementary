<?php declare(strict_types = 1);

namespace Elementary\Http;

use Elementary\Http\Exceptions;

class Request
{
    /**
     * $_POST array.
     * 
     * @var array
     */
    protected $post;

    /**
     * $_GET array.
     * 
     * @var array
     */
    protected $get;

    /**
     * $_COOKIE array.
     * 
     * @var array
     */
    protected $cookies;

    /**
     * $_FILES array.
     * 
     * @var array
     */
    protected $files;

    /**
     * $_SERVER array.
     * 
     * @var array
     */
    protected $server;

    /**
     * Contents of php://input
     * 
     * @var string
     */
    protected $inputStream;

    /**
     * Create new request instance.
     * 
     * @param array $post
     * @param array $get
     * @param array $cookies
     * @param array $files
     * @param array $server
     */
    public function __construct(array $post, array $get, array $cookies, array $files, array $server, string $inputStream = '')
    {
        $this->post = $post;
        $this->get = $get;
        $this->cookies = $cookies;
        $this->files = $files;
        $this->server = $server;
        $this->inputStream = $inputStream;
    }

    /**
     * Return $_GET parameter or default if does not exist.
     * 
     * @param  string $key
     * @param  mixed $default
     * @return string
     */
    public function getQuery(string $key, $default = null): ?string
    {
        return $this->getRequestVariable($key, 'get', $default);
    }

    /**
     * Get all query parameters.
     * 
     * @return array
     */
    public function getQueryAll(): array
    {
        return $this->get;
    }

    /**
     * Return $_POST parameter or a default value if none is set.
     * 
     * @param  string $key
     * @param  mixed $default
     * @return string
     */
    public function getPost(string $key, $default = null): ?string
    {
        return $this->getRequestVariable($key, 'post', $default);
    }

    /**
     * Get all post parameters.
     * 
     * @return array
     */
    public function getPostAll(): array
    {
        return $this->post;
    }

    /**
     * Get file parameter or a default value if none is set.
     * 
     * @param  string $key 
     * @param  mixed $default
     * @return string
     */
    public function getFile(string $key, $default = null): ?string
    {
        return $this->getRequestVariable($key, 'files', $default);
    }

    /**
     * Get all files.
     * 
     * @return array
     */
    public function getFiles(): array
    {
        return $this->files;
    }

    /**
     * Get cookie parameter or a default value if none is set.
     * 
     * @param  string $key
     * @param  mixed $default
     * @return string
     */
    public function getCookie(string $key, $default = null): ?string
    {
        return $this->getRequestVariable($key, 'cookies', $default);
    }

    /**
     * Get all cookies.
     * 
     * @return array
     */
    public function getCookies(): array
    {
        return $this->cookies;
    }

    /**
     * Get raw body from input stream.
     * 
     * @return string
     */
    public function getRawBody(): string
    {
        return $this->inputStream;
    }

    /**
     * Get REQUEST_URI.
     * 
     * @return string
     */
    public function getUri(): string
    {
        return $this->getServerVariable('REQUEST_URI');
    }

    /**
     * Get request method.
     * 'GET', 'POST', 'HEAD', 'PUT'
     * 
     * @return string
     */
    public function getMethod(): string
    {
        return $this->getServerVariable('REQUEST_METHOD');
    }

    /**
     * Shorthand for checking if method is post.
     * 
     * @return boolean
     */
    public function isPost(): bool
    {
        return $this->getServerVariable('REQUEST_METHOD') == 'POST' ? true: false;
    }

    /**
     * Shorthand for checking if method is get.
     * 
     * @return boolean
     */
    public function isGet(): bool
    {
        return $this->getServerVariable('REQUEST_METHOD') == 'GET' ? true: false;
    }

    /**
     * Shorthand for checking if method is head.
     * 
     * @return boolean
     */
    public function isHead(): bool
    {
        return $this->getServerVariable('REQUEST_METHOD') == 'HEAD' ? true: false;
    }

    /**
     * Shorthand for checking if method is put.
     * 
     * @return boolean
     */
    public function isPut(): bool
    {
        return $this->getServerVariable('REQUEST_METHOD') == 'PUT' ? true: false;
    }

    /**
     * Contents of HTTP_ACCEPT.
     * 
     * @return string
     */
    public function getHttpAccept(): string
    {
        return $this->getServerVariable('HTTP_ACCEPT');
    }

    /**
     * Contents of HTTP_REFERER.
     * 
     * @return string
     */
    public function getReferer(): string
    {
        return $this->getServerVariable('HTTP_REFERER');
    }

    /**
     * Contents of HTTP_USER_AGENT.
     * 
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->getServerVariable('HTTP_USER_AGENT');
    }

    /**
     * Get ip of the user.
     * 
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->getServerVariable('REMOTE_ADDR');
    }

    /**
     * Check if request is over ssl.
     * 
     * @return string
     */
    public function isSecure(): string
    {
        return (array_key_exists('HTTPS', $this->server) && $this->server['HTTPS'] !== 'off');
    }

    /**
     * Get full query string.
     * 
     * @return string
     */
    public function getQueryString(): string
    {
        return $this->getServerVariable('QUERY_STRING');
    }

    /**
     * Checks if key is set and returns it.
     * 
     * @param  string $key
     * @param  string $variable
     * @param  mixed $default
     * @return string
     */
    private function getRequestVariable(string $key, string $variable, $default): ?string
    {
        if (array_key_exists($key, $this->$variable)) {
            return $this->$variable[$key];
        }

        return $default;
    }

    /**
     * Check if key exists on server variable and return it.
     * 
     * @param  string $key
     * @return string
     */
    private function getServerVariable(string $key): string
    {
        if (!array_key_exists($key, $this->server)) {
            throw new MissingServerVariableException($key);
        }

        return $this->server[$key];
    }
}