<?php declare(strict_types = 1);

namespace App\DataMappers;

use App\Entities\User;
use PDO;

class UserMapper
{
    /**
     * PDO object.
     * 
     * @var PDO
     */
    private $pdo;

    /**
     * Set PDO.
     * 
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Get user by id.
     * 
     * @param  int $id
     * @return User|bool
     */
    public function findById(int $id): ?User
    {
        $stmt = $this->pdo->prepare("SELECT id, name, email FROM users WHERE id = :id");
        $stmt->execute(['id' => $id]);
        $stmt->setFetchMode(PDO::FETCH_CLASS, User::class);

        return $stmt->fetch();
    }

    public function save(User $user)
    {

    }
}