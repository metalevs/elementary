<?php

namespace App\Entities;

class User
{
    /**
     * User primary id.
     * 
     * @var int
     */
    private $id;

    /**
     * User name.
     * 
     * @var string
     */
    private $name;

    /**
     * User email.
     * 
     * @var string
     */
    private $email;

    /**
     * Fill user entity with data.
     * 
     * @param array|null $data
     */
    public function __construct(array $data = null)
    {
        if (is_array($data)) {
            $this->id = $data['id'];
            $this->name = $data['name'];
            $this->email = $data['email'];
        }
    }

    /**
     * Retrive user id.
     * 
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Retrieve user name.
     * 
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Retrieve user email.
     * 
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}