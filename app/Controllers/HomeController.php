<?php declare(strict_types = 1);

namespace App\Controllers;

use Elementary\Http\Response;
use Elementary\Http\Request;
use Elementary\View\Template;
use App\DataMappers\UserMapper;

class HomeController
{
    /**
     * Template engine.
     * 
     * @var Elementary\View\Template
     */
    private $view;

    /**
     * Response object
     * 
     * @var Elementary\Http\Response
     */
    private $response;

    /**
     * Request object.
     * 
     * @var Elementary\Http\Request
     */
    private $request;

    /**
     * User repository.
     * 
     * @var App\DataMappers\UserMapper
     */
    private $userMapper;

    /**
     * Construct.
     * 
     * @param Template $view
     */
    public function __construct(Template $view, Response $response, Request $request, UserMapper $userMapper)
    {
        $this->response = $response;
        $this->view = $view;
        $this->request = $request;
        $this->userMapper = $userMapper;
    }

    /**
     * Homepage.
     * 
     * @return Elementary\Http\Response
     */
    public function index(): Response
    {
        $this->response->setContent($this->view->render('home/index'));
        return $this->response;
    }

    /**
     * About page.
     * 
     * @return Response
     */
    public function about(): Response
    {
        $this->response->setContent($this->view->render('home/about'));
        return $this->response;
    }

    public function create(): Response
    {
        $user = $this->userMapper->findById(2);
        dd($user->getName());
    }
}